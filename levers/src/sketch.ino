const int LEVER_PINS[] = {3, 4, 5, 6, 7};
const int RELAY_PIN = 13;

void setup() {
    pinMode(RELAY_PIN, OUTPUT);
    for (int i = 0; i < 5; i++) {
        pinMode(LEVER_PINS[i], INPUT_PULLUP);
    }
}

void loop() {
    static bool powered = true;

    int values[5];
    for (int i = 0; i < 5; i++) {
        values[i] = digitalRead(LEVER_PINS[i]);
    }

    bool valid = true;
    for (int i = 0; i < 5; i++) {
        if (!values[i]) {
            valid = false;
            break;
        }
    }

    if (valid && !powered) {
        digitalWrite(RELAY_PIN, HIGH);
        powered = true;
    } else if (!valid && powered) {
        digitalWrite(RELAY_PIN, LOW);
        powered = false;
    }
}
