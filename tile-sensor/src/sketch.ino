#define SENSOR_PIN A1
#define RELAY_PIN 13
#define TRIGGER_THRESHOLD 512
#define SAMPLES 10
#define POLL_TIME 25

void setup() {
    Serial.begin(115200);
    pinMode(RELAY_PIN, OUTPUT);
}

void loop() {
    static bool powered = true;
    const int value = readAvg(SENSOR_PIN, SAMPLES, POLL_TIME);

    if (powered && value > TRIGGER_THRESHOLD) {
        powered = false;
        digitalWrite(RELAY_PIN, powered);
    } else if (!powered && value < TRIGGER_THRESHOLD) {
        powered = true;
        digitalWrite(RELAY_PIN, powered);
    }
}

int readAvg(const int pin, const int count, const int time) {
    long sum = 0;
    for (int i = 0; i < count; i++) {
        sum += analogRead(pin);
        delay(time);
    }
    return sum / count;
}
