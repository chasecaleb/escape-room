#ifndef _clock_h
#define _clock_h
#include <Digits.h>

class Clock : public Digits {
    public:
        Clock(int dataPin, int clockPin, int latchPin);
        void incrementTime(int minutes);
        void updateDisp();
        int time;
};

#endif
