#include "clock.h"

Clock::Clock(int dataPin, int clockPin, int latchPin) : Digits(dataPin, clockPin, latchPin) {
    time = 0;
    updateDisp();
}

void Clock::incrementTime(int amount) {
    time += amount;
    const int minsInDay = 60*24;
    time = ((time % minsInDay) + minsInDay) % minsInDay;
    updateDisp();
}

void Clock::updateDisp() {
    // Colon separating hours/minutes
    bool dots[4] = {false, true, true, false};
    int digits[4];
    const int hours = time / 60;
    const int minutes = time % 60;

    digits[0] = hours / 10;
    digits[1] = hours % 10;
    digits[2] = minutes / 10;
    digits[3] = minutes % 10;

    show(digits, dots, 4);
}
