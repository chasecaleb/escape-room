#include "clock.h"

// LED/clock config
#define REPEAT_TIME 50  // Time in milliseconds between repeats while button held.
#define INITITAL_REPEAT_TIME 300
#define DATA_PIN 2
#define LATCH_PIN 3
#define CLK_PIN 4
#define ENABLE_PIN 6  // Used as brightness control via PWM
#define BRIGHTNESS 150 // Enable pin duty cycle, 0-255, lower=brighter
// Radio config
#define BUTTON_ONE_PIN A3  // Increment by one minute
#define BUTTON_TWO_PIN A2  // Decrement by one minute
#define BUTTON_THREE_PIN A1  // Only on master remote, reset time to zero
#define BUTTON_FOUR_PIN 13  // Only on master remote, turn relay on for 1 minute
// Relay
#define RELAY_PIN 11
const int TRIGGER_TIME = 320;  // Time in minutes to trigger relay at
const unsigned long TRIGGER_LENGTH = 30L * 60L * 1000L;  // Time to hold relay open, min * sec * ms
const unsigned long OVERRIDE_LENGTH = 1L * 60L * 1000L;

Clock clock(DATA_PIN, CLK_PIN, LATCH_PIN);
unsigned long lastTick = 0;
bool triggered = false;
unsigned long triggerEnd;

void setup() {
    Serial.begin(115200);
    pinMode(BUTTON_ONE_PIN, INPUT);
    pinMode(BUTTON_TWO_PIN, INPUT);
    pinMode(BUTTON_THREE_PIN, INPUT);
    pinMode(BUTTON_FOUR_PIN, INPUT);
    pinMode(ENABLE_PIN, OUTPUT);
    analogWrite(ENABLE_PIN, BRIGHTNESS);
    digitalWrite(RELAY_PIN, LOW);
}

void loop() {
    // Regular clock tick every minute
    if (millis()/1000 > lastTick/1000 + 60) {
        clock.incrementTime(1);
        lastTick = millis();
        Serial.print("tick ");
        Serial.print(" ");
        Serial.println(clock.time);
    }

    // Handle remote button presses (increment/decrement)
    checkButton(BUTTON_ONE_PIN, 1);
    checkButton(BUTTON_TWO_PIN, -1);

    // Master buttons: reset time, temporarily override lock.
    if (digitalRead(BUTTON_THREE_PIN)) {
        clock.time = 0;
        clock.updateDisp();
        if (triggered) {
            digitalWrite(RELAY_PIN, LOW);
            triggered = false;
        }
        Serial.println("Clock master reset triggered");
    }
    if (digitalRead(BUTTON_FOUR_PIN)) {
        triggerEnd = millis() + OVERRIDE_LENGTH;
        digitalWrite(RELAY_PIN, HIGH);
        triggered = true;
        Serial.println("Door master override triggered");
    }

    checkTime();
}

void checkButton(int pin, int incrementAmt) {
    int holdCount = 0;
    while(digitalRead(pin)) {
        if (triggered) {
            triggered = false;
            digitalWrite(RELAY_PIN, LOW);
            Serial.println("changed");
        }

        clock.incrementTime(incrementAmt);
        Serial.println(clock.time);
        lastTick = millis();
        if (holdCount < 5) {
            delay(INITITAL_REPEAT_TIME);
        } else {
            delay(REPEAT_TIME);
        }
        holdCount++;
    }
}

void checkTime() {
    /* static unsigned long triggerEnd; */
    /* static unsigned long triggerTime = 0; */
    /* const unsigned long triggerEnd = triggerTime + TRIGGER_LENGTH; */
    if (!triggered && clock.time == TRIGGER_TIME) {
        triggered = true;
        triggerEnd = millis() + TRIGGER_LENGTH;
        /* triggerEnd =  */
        /* triggerTime = millis(); */
        digitalWrite(RELAY_PIN, HIGH);
        Serial.println("triggering");
    } else if (triggered && millis() > triggerEnd) {
        triggered = false;
        digitalWrite(RELAY_PIN, LOW);
        Serial.println("trigger timeout");
    }
}
